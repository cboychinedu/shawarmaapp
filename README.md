# Shawarma Express App

<img src="./src/Assets/ReadmeImages/img.jpg" />

<p> Shawarma Express is a web and mobile application that allows users to easily order their favorite shawarma and other delicious food items and have them delivered to their doorstep. </p>

## Features

- Browse a diverse menu of shawarma and other food items.
- Place orders with customizable options.
- Specify delivery location and payment method.
- Real-time order tracking.
- Notifications for order status updates.
- Secure payment processing.
- User profiles and order history.

## Technologies Used

- **Frontend**:

  - [React.js](https://reactjs.org/)
  - [React Native](https://reactnative.dev/) (for mobile app)
  - [Styled-components](https://styled-components.com/) for styling
  - [React Router](https://reactrouter.com/) for web routing
  - [Redux](https://redux.js.org/) for state management

- **Backend**:
  - Node.js and Express.js for the server
  - MongoDB for the database
  - [Socket.io](https://socket.io/) for real-time order updates
  - [Stripe](https://stripe.com/) for payment processing
  - [Firebase](https://firebase.google.com/) for authentication

## Installation and Usage

1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/shawarma-express-app.git
   cd shawarma-express-app
   ```
