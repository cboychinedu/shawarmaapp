// Creating a function for calculating the total price 
const calculateTotalPrice = (shawarmaList) => {
    // Setting the initial price 
    let totalPrice = 0; 

    // Looping through the shawarmaList 
    for (const shawarma of shawarmaList) {
        // Parse the 'Price' string as a float and add it to 
        // the total price 
        totalPrice += Number.parseFloat(shawarma.Price); 

    }

    // Return the total price 
    return totalPrice; 
}

// Exporting the function 
module.exports = calculateTotalPrice; 