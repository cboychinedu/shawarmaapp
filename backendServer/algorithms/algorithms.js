// Creating a function for shuffling an array 
let shuffleArray = (data) => {
	// Create a copy of the original array to avoid modifying it directly 
	const shuffledData = [...data]; 

	// Looping 
	for (let i = shuffledData.length - 1; i > 0; i--) {
		// Generate a random index 
		const randomIndex = Math.floor(Math.random() * (i + 1));

		// Swap element at i and randomIndex 
		[shuffledData[i], shuffledData[randomIndex]] = [shuffledData[randomIndex], shuffledData[i]];
 
	}

	// Returning the shufled data 
	return shuffledData; 
}

// Exporting the modules 
module.exports = shuffleArray; 