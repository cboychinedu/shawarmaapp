// Getting all the dom elements 
const nameValue = document.getElementById("name")
const description = document.getElementById("description"); 
const fileUpload = document.getElementById("fileUpload"); 
const price = document.getElementById("price"); 
const uploadBtn = document.getElementById("uploadBtn"); 

// Adding event listener for the upload Btn 
uploadBtn.addEventListener("click", (event) => {
    // Creating a form data 
    const formData = new FormData(); 

    // Appending 
    formData.append('myFile', fileUpload.files[0]); 
    formData.append('name', nameValue.value); 
    formData.append('description', description.value); 
    formData.append('price', price.value); 

    // Sending the form data using fetch 
    fetch('/admin/uploadShawarmaData', {
        method: 'POST', 
        body: formData
    })
    .then((response) => response.json())
    .then((responseData) => {
        // If the data was a success 
        if (responseData.status === "success") {
            alert("Data uploaded")
        }

        // 
        else {
            alert("error")
        }
    })

})