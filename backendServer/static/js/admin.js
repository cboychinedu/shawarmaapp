// Getting the dom element for the email address and password
const email = document.getElementById("email"); 
const password = document.getElementById("password"); 
const loginBtn = document.getElementById("loginBtn");

// Adding event listener for the login button
loginBtn.addEventListener('click', (event) => {
    // Prevent default user submission 
    event.preventDefault();
    
    // Prevent default user submission if the forms are not validate 
    if (!email.value) {
        alert("Please enter your email address");
        return;
    }

    // else if the form is not validi for the password field. 
    else if (!password.value) {
        alert("Please enter your password");
        return;
    }

    // else 
    else {
        // Get the user email address, and password 
        const emailAddress = email.value; 
        const passwordValue = password.value;

        // Making fetch request to the server 
        fetch('/admin', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                emailAddress: emailAddress,
                password: passwordValue
            })
        })
        .then((response) => response.json())
        .then((responseData) => {
            // Log the data 
            console.log(responseData); 

            // If the status was success 
            if (responseData.status === "success") {
                setInterval(()=> {
                    window.location.href = "/admin/uploadShawarmaData"
                }, 2000)
            }
        })


    } 
})