// Getting the dom elements 
const fullname = document.getElementById("fullname"); 
const email = document.getElementById("email"); 
const encKey = document.getElementById("encKey"); 
const password = document.getElementById("password"); 
const registerBtn = document.getElementById("registerBtn"); 

// Adding event listeners for the register button 
registerBtn.addEventListener("click", (event) => {
    // Preventing default submission 
    event.preventDefault(); 

    // Checking the forms 
    if (!fullname.value) {
        // Display an alert box 
        alert("Fullname field missing"); 
    }

    // Checking the email 
    else if (!email.value) {
        // Display the alert box 
        alert('Email is required'); 
    }

    // Checking the enc key 
    else if (!encKey.value) {
        // Display the alert box 
        alert('Encryption key is required'); 
    }

    // Checking if the password value is valid 
    else if (!password.value) {
        // Display the alert box 
        alert('Password key is required'); 
    } 

    // Else if all conditions were met. 
    else {
        // Save the admin user's data as 
        // a json object 
        let adminUserData = JSON.stringify({
            "fullname": fullname.value, 
            "emailAddress": email.value, 
            "encKey": encKey.value, 
            "password": password.value, 
        })

        // Sending the admin user data as a post request 
        // to the 
        fetch('/admin/registerAdmin', {
            method: 'POST', 
            headers: { 'Content-Type': 'application/json'}, 
            body: adminUserData, 
        })
        .then((response) => response.json())
        .then((responseData) => {

            // Checking the data if success 
            if (responseData.status === "success") {
                // Setting a timeout 
                setTimeout(() => {
                    // Redirecting the user to the login page 
                    window.location.href = '/admin'; 
                }, 1000)
            }

            // else the status error 
            else if (responseData.status === "error") {
                // Display the error message 
                alert(responseData.message); 
            }
        })
    }
})