const bcrypt = require('bcryptjs'); 

// 
const salt = bcrypt.genSaltSync(10); 
const hashedPassword = bcrypt.hashSync('password', salt); 

console.log(hashedPassword); 

// 
const isMatch = bcrypt.compareSync('password1', hashedPassword); 
console.log(isMatch); 