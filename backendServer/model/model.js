// Importing the necessary modules 
const mongodb = require('mongoose'); 

// Creating a schema for the users 
const userSchema = new mongodb.Schema({
    fullname: { type: String }, 
    emailAddress: { type: String }, 
    phoneNumber: { type: String }, 
    password: { type: String }, 
    date: { type: Date, default: Date.now } 
}); 

// Creating a schema for the admin users 
const adminSchema = new mongodb.Schema({
    fullname: { type: String }, 
    emailAddress: { type: String }, 
    password: { type: String }, 
    date: { type: Date, default: Date.now } 
}); 

// Cart Schema 
const productsSchema = new mongodb.Schema({
    Name: { type: String }, 
    Description: { type: String }, 
    ImageUrl: { type: String }, 
    Price: { type: String },
    paymentVerified: {type: String}, 
})

// Creating the main cart schema 
const mainCartSchema = new mongodb.Schema({
    items: [productsSchema], 
    totalFee: { type: String }, 
    purchased: { type: String, default: "false"}, 
}); 

// Creating the users collection 
const USERS = mongodb.model('users', userSchema); 
const ADMIN = mongodb.model('admin', adminSchema);
const CART = mongodb.model('cart', mainCartSchema); 
const PRODUCTS = mongodb.model('products', productsSchema); 

// Exporting the modules 
module.exports = { USERS, CART, PRODUCTS, ADMIN }; 