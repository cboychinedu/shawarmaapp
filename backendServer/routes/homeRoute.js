// Importing the necessary modules 
const express = require('express'); 
const jwt = require('jsonwebtoken'); 
const bcrypt = require('bcryptjs'); 
const { USERS } = require('../model/model'); 

// Creating the router object 
const router = express.Router(); 

// Creating a route for registering users 
router.post('/register', async (req, res) => {
    // Using the try catch block to connect to the database 
    try {
        // Searching the database to see if the user with the specified 
        // email address is registered on the database 
        let user = await USERS.findOne({
            emailAddress: req.body.emailAddress 
        })

        // If the user exists on the database, execute the 
        // block of code below 
        if (user) {
            // Create an error message 
            let errorMessage = JSON.stringify({
                "message": "The user with the email address is already registered", 
                "status": "user-registered-error"
            }); 

            // Sending the error message 
            return res.send(errorMessage); 
        }

        // If the email for the user was not found on the database, execute the 
        // block of code below 
        else {
            // Encrypt the password, and create a salt hash 
            const salt = await bcrypt.genSalt(10); 
            const hashedPassword = bcrypt.hashSync(req.body.password, salt); 

            // Saving the new registered user 
            const newUser = new USERS({
                fullname: req.body.fullname, 
                emailAddress: req.body.emailAddress, 
                phoneNumber: req.body.phoneNumber, 
                password: hashedPassword, 
            })

            // Saving the new user on the database 
            await newUser.save(); 

            // Generating the success message 
            let successMessage = JSON.stringify({
                "message": "User newly registered", 
                "status": "success"
            }); 

            // Return the success message 
            return res.send(successMessage); 
        }
    }

    // Catch the error 
    catch (error) {
        // Getting the error message 
        let errorMessage = JSON.stringify({
            "message": error.toString().trim(), 
            "status": "error"
        }); 

        // Sending back the error message 
        return res.send(errorMessage); 
    }
    
})

// Creating a route for logging in registered users 
router.post('/login', async(req, res) => {   
    // Searching the database to see if the user with the 
    // specified email address is registered on the database
    try {
        // Get the user details 
        const user = await USERS.findOne({
            emailAddress: req.body.emailAddress
        })

        // If the email address specified was not found on the database, 
        // execute the block of code below 
        if (!user) {
            // Create the error message 
            let errorMessage = JSON.stringify({
                "message": "Invalid email or password", 
                "status": "error", 
                "statusCode": 401, 
            })

            // Sending the error message 
            return res.send(errorMessage); 
        }

        // Else if the user exists 
        else {
            // Get the user password, and the hashPassword 
            const userPassword = req.body.password; 
            const hashPassword = user.password; 

            // Checking if the hashed value is correct 
            const isMatch = bcrypt.compareSync(userPassword, hashPassword); 

            // Getting the secret key 
            const jwtKey = process.env.jwtKey; 

            // Checking if the passwords are correct 
            if (isMatch) {
                // Create a JWT token 
                const token = jwt.sign({
                    email: user.emailAddress, 
                    isLoggedIn: true, 
                    id: user._id
                }, jwtKey, {
                    expiresIn: '30 days'
                }); 

                // Building the success message 
                let successMessage = JSON.stringify({
                    "message": "Logged in successfully", 
                    "status": "success", 
                    "x-auth-token": token, 
                    "statusCode": 200, 
                })

                // Sending the success message 
                return res.send(successMessage); 
            }

            // Else if the passwords didn't match 
            else {
                // Create an error message body 
                let errorMessage = JSON.stringify({
                    "message": "Invalid email or password", 
                    "status": "error", 
                    "statusCode": 404, 
                })

                // Sending back the error message 
                return res.send(errorMessage); 
            }
        }
    } 

    // On error connecting to the database, catch the error 
    catch (error) {
        // Create the error message 
        let errorMessage = JSON.stringify({
            "message": error.toString().trim(), 
            "status": "error", 
            "statusCode": 500, 
        })

        // Sending back the error message 
        return res.send(errorMessage); 
    }

})

// Creating a route for getting the user details 

// Exporting the router 
module.exports = router; 