// Importing the necesary modules 
const express = require('express'); 
const jwt = require('jsonwebtoken'); 
const path = require('path'); 
const { PRODUCTS } = require('../model/model'); 
const { rootPath } = require('../base'); 

// Creating the router object 
const router = express.Router(); 

// Creating a route for retrival of the image 
router.get('/:filename', async (req, res) => {
    // Getting the file name 
    const filename = req.params.filename; 

    // Getting the file path 
    const fullPath = path.join(rootPath, 'static', 'imageData', filename); 

    // Rendering the image 
    return res.sendFile(fullPath); 

})


// Exporting the router 
module.exports = router; 