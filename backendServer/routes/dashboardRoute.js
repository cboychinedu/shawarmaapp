// Importing the necessary modules 
const express = require('express'); 
const path = require('path'); 
const jwt = require('jsonwebtoken'); 
const protectedRoute = require('../auth/auth'); 
const { USERS } = require('../model/model'); 

// Getting the jwt key 
const jwtkey = process.env.jwtkey; 

// Creating the route object 
const router = express.Router(); 

// Creating a route for updating the users information 
router.post('/updateuser', protectedRoute, async (req, res, next) => {
    // Using try catch block to get the user's details 
    try {
        // Get the user's token value 
        const usersToken = req.body.tokenValue; 

        // Decode the user's token 
        const decodedTokenData = jwt.decode(usersToken, jwtkey); 

        // Get the user's data from the database 
        const { fullname, emailAddress, phoneNumber } = req.body; 

        // Update the user's information in the database 
        const result = await USERS.findOneAndUpdate({
            emailAddress: decodedTokenData.email 
        }, {
            fullname, 
            emailAddress, 
            phoneNumber
        }); 

        // Building the response message 
        const responseBody = JSON.stringify({
            "status": "success", 
            "message": "User information updated successfully", 
            "statusCode": 200, 
        }); 

        // Sending the responseBody as a message 
        return res.send(responseBody); 
    }

    // Catch the error 
    catch (error) {
        // Building an error message 
        let errorMessage = JSON.stringify({
            "status": "error", 
            "message": error.toSring().trim(), 
            "statusCode": 500, 
        }); 

        // Sending the error message 
        return res.send(errorMessage).status(500); 
    }
})

// Creating a route for getting the users 
router.post('/getusers', protectedRoute, async(req, res, next) => {

    // Getting the user's token value 
    const usersToken = req.body.tokenValue; 

    // Decoding the user's token 
    const decodedTokenData = jwt.decode(usersToken, jwtkey); 

    // Getting the user's data from the database 
    const userData = await USERS.find({
        emailAddress: decodedTokenData.email, 
    })
    .select({
        fullname: 1,
        emailAddress: 1,
        phoneNumber: 1,
    })

    // Creating the response body 
    const responseBody = JSON.stringify({
        "status": "success", 
        "body": userData, 
        "statusCode": 200, 
    }); 

    // Sending the data 
    return res.send(responseBody);

})

// Exporting the route 
module.exports = router; 



