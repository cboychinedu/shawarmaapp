// Importing the necessary modules 
const express = require('express'); 
const jwt = require('jsonwebtoken'); 
const path = require('path');
const protectedRoute = require('../auth/auth'); 
const calculateTotalPrice = require('../algorithms/calculatePrice'); 
const { CART, PRODUCTS } = require('../model/model'); 

// Creating the router object 
const router = express.Router(); 

// Getting the jwt key 
const jwtKey = process.env.jwtKey

// Creating a route for deleting an item from the cart 
router.post('/deleteCartItem', protectedRoute, async(req, res, next) => {
    // Using try catch block logic 
    try {
        // Get the cart token and item to delete from the request body 
        const { cartToken, itemIdToDelete } = req.body; 

        // Decode the token 
        const decodedToken = jwt.verify(cartToken, jwtKey); 

        // Getting the data from the database 
        let cartSelectedData = await CART.findOne({
            _id: decodedToken.id
        })

        // If the cart is not present 
        if (!cartSelectedData) {
            // Creating an error message 
            let errorMessage = JSON.stringify({
                "status": "error", 
                "message": "Cart not found", 
                "statusCode": 404, 
            }); 

            // Sending the error message 
            return res.send(errorMessage); 
        }

        // else if the cart is present 
        else {
            // Check if the item exists in the cart 
            const itemIndex = cartSelectedData.items.findIndex((item) => item._id == itemIdToDelete); 

            // If the item was not found in the cart 
            if (itemIndex === -1) { 
                // Building the response message 
                let responseMessage = JSON.stringify({
                    "status": "error", 
                    "message": "Item not found in the cart", 
                    "statusCode": 404, 
                }); 

                // Sending the response message 
                return res.send(responseMessage); 
            }

            // Else if the cart item was found, remove the item from the cart
            cartSelectedData.items.splice(itemIndex, 1); 

            // Recalculate the total fee 
            const totalFee = calculateTotalPrice(cartSelectedData.items); 

            // Update the cart in the database 
            await CART.findByIdAndUpdate(decodedToken.id, {
                items: cartSelectedData.items, 
                totalFee
            }); 

            // Building a success message 
            let successMessage = JSON.stringify({
                "status": "success", 
                "message": "Item removed from the cart", 
                "statusCode": 200
            }); 

            // Sending the success message 
            return res.send(successMessage); 
        }

    }

    // Catch the error 
    catch(error) {
        // Building the error message 
        let errorMessage = JSON.stringify({
            "status": "error", 
            "message": error.toString().trim(), 
            "statusCode": 500, 
        }); 

        // Sending the error message 
        return res.send(errorMessage); 
    }
})


// Creating a route for "Creating the Cart"
router.post('/createCart', protectedRoute, async(req, res, next) => {

    // Using try catch block 
    try {
        // Getting the data from the request body  
        const requestData = req.body; 

        // Getting the data by looping through all the cart id
        const results = await Promise.all(requestData.cartItems.map(async (id) => {
            // Fetching the full data about the cart id 
            try {
                const products = await PRODUCTS.findOne({ _id: id})
                .select({
                    _id: 1, 
                    Name: 1, 
                    Description: 1, 
                    Price: 1, 
                    ImageUrl: 1

                }); 

                // Returning the products 
                return products; 
            }
            // Catch 
            catch (error) {
                console.error("Error fetching the data "); 
                return null; 
            }
        }))

        // Getting the total fee
        const totalFee = calculateTotalPrice(results); 

        // If the cart is empty, execute the block of code below 
        // Saving the cart on the database  
        let cartData = new CART({
            items: results, 
            totalFee: totalFee, 
        })

        // Saving the result on the database 
        const result = await cartData.save(); 

        // Signing a session token 
        const token = jwt.sign({
            id: result._id, 
        }, jwtKey, {
            expiresIn: "20 days", 
        }); 

        // Sending the token back as a url 
        let successMessage = JSON.stringify({
            "message": "Cart created", 
            "cartToken": token, 
            "status": "success", 

        })

        // Sending the success message 
        return res.send(successMessage); 

        
    }

    // Catch 
    catch(error) {
        // Create an error message 
        let errorMessage = JSON.stringify({
            "message": error.toString().trim(), 
            "status": "error", 
            "statusCode": 404, 
        })

        // sending back the error message 
        return res.send(errorMessage); 
    }
})

// Creating a route for getting the Cart 
router.post('/getCart', protectedRoute, async(req, res, next) => {
    // Getting the cart token from the post request body 
    let cartToken = req.body.tokenValue;

    // Decode the token 
    const decodedToken = jwt.verify(cartToken, jwtKey); 

    // GEtting the data from the database 
    let cartSelectedData = await CART.findOne({
        _id: decodedToken.id
    })

    // Returning it to the user 
    return res.send(cartSelectedData); 

})

// Exporting the router 
module.exports = router; 