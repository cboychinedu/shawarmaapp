// Importing the necessary modules 
const express = require('express'); 
const path = require('path'); 
const bcrypt = require('bcryptjs'); 
const { rootPath } = require('../base'); 
const adminProtectedRoute = require('../auth/adminAuth'); 
const { PRODUCTS, ADMIN } = require('../model/model')

// Creating the router object 
const router = express.Router(); 

// Loading the env variables 
require('dotenv').config(); 

// Creating the session variable 
var sess; 

// Creating get a route for the admin page 
router.get('/', async (req, res) => {
    // Setting the full path to the html template file 
    let fullPath = path.join(rootPath,'static', 'templates', 'admin.ejs'); 

    // Render 
    return res.render(fullPath); 
}); 

// Creating a post route for the admin page
router.post('/', async (req, res) => {
    // Using try catch block 
    try {
        // Searching if the user is registered on the admin database 
        let adminUser = await ADMIN.findOne({
            emailAddress: req.body.emailAddress
        });  

        // If the email address specified was not found on the database 
        if (!adminUser) {
            // Create the error message 
            let errorMessage = JSON.stringify({
                "message": "Invalid email or password provided", 
                "status": "error", 
                "statusCode": 404, 
            }); 

            // Sending back the error message 
            return res.send(errorMessage).status(404);
        }

        // Else if the email address was found on the server 
        else {
            // Get the user password and hashed password 
            let adminPassword = req.body.password; 
            let hashedPassword = adminUser.password; 

            // Comparing the password to see if it is valid 
            let passwordCondition = await bcrypt.compare(adminPassword, hashedPassword); 

            // Sending back a response if the password is validated correctly
            if (passwordCondition) {
                // Create the user session 
                sess = req.session; 
                sess.emailAddress = req.body.emailAddress;
                sess._id = adminUser._id; 
                sess.isAuth = true; 

                // Sending the response for the successful connection 
                let successMessage = JSON.stringify({
                    "message": "User logged in successfully", 
                    "status": "success", 
                    "statusCode": 200, 
                }); 

                // Sending back the success message 
                return res.send(successMessage).status(200); 
            }

            // Else if the password condtion was false 
            else {
                // Create an error message 
                let errorMessage = JSON.stringify({
                    "message": "Invalid email or password", 
                    "status": "error", 
                    "statusCode": 404, 
                }); 

                // Sending back the error message 
                return res.send(errorMessage); 
            }
        }
    } 

    // Catch the error 
    catch(error) {
        // On error connecting to the database, execute the block of code 
        // below 
        let errorMessage = JSON.stringify({
            "message": error.toString().trim(), 
            "status": "error", 
            "statusCode": 500, 
        }); 

        // Sending back the error message 
        return res.send(errorMessage).status(500); 
    }
    
})

// Creating a get route for registering the admin user with the specified 
// encryption key and password for a post route 
router.post('/registerAdmin', async (req, res, next) => {
    // Using try catch block to get the user's input data 
    try {
        // Searching the database to see if the admin user is registered on the database 
        let adminUser = await ADMIN.findOne({
            emailAddress: req.body.emailAddress
        }); 

        // Getting the encryption key 
        const encKey = process.env.encKey; 

        // Getting the admin user's specified encKey 
        const adminUserEncKey = req.body.encKey; 

        // If the admin user exists on the database create an error 
        // message 
        if (adminUser) {
            // Creating an error message 
            let errorMessage = JSON.stringify({
                "message": "User already registered on the database", 
                "status": "error", 
                "statusCode": 500 
            }); 

            // Sending back the error message 
            return res.send(errorMessage); 
        }

        // Else if the email for the user was not found, and the secret key was specified 
        else if ( (!adminUser) && (encKey === adminUserEncKey)) {
            // Encrypt the password, connect to the database and save the admin user 
            let salt = await bcrypt.genSalt(10); 
            let hashedPassword = await bcrypt.hash(req.body.password, salt); 

            // Saving the newly registered admin user 
            let registeredUser = new ADMIN({
                fullname: req.body.fullname, 
                emailAddress: req.body.emailAddress, 
                password: hashedPassword,  
            })

            // Saving the admin user 
            await registeredUser.save(); 

            // Create a success message, and send it back to the client 
            let successMessage = JSON.stringify({
                "message": "User registred on the database", 
                "status": "success", 
                "statusCode": 200, 

            }); 

            // Sending back the success message 
            return res.send(successMessage).status(200); 
        }

        // Else create an error messag and send it back to the user 
        else {
            // Creating the error message 
            let errorMessage = JSON.stringify({
                "message": "Incorrect encryption key", 
                "status": "error", 
                "statusCode": 404, 
            })

            // Sending back the erro message 
            return res.send(errorMessage); 
        }
    }

    // Catch 
    catch (error) {
        // Creatting en error message 
        let errorMessage = JSON.stringify({
            "message": error.toString().trim(), 
            "status": "error", 
            "statusCode": 500,
        }); 

        // Sending back the error message 
        return res.send(errorMessage); 
    }
})


// Creating the route for registering the admin user with the specified encryption key and 
// password.
router.get('/registerAdmin', async (req, res, next) => {
    // Setting the full path to the html template file 
    let fullPath = path.join(rootPath, 'static', 'templates', 'registerAdmin.ejs'); 

    // Rendering the full path to the register admin template
    return res.render(fullPath)
}); 


// Creating a get route for posting the shawarma data 
router.get("/uploadShawarmaData", adminProtectedRoute, async(req, res) => {
    // Setting the full path to the html template file 
    let fullPath = path.join(rootPath, 'static', 'templates', 'uploadProducts.ejs'); 

    // Render 
    return res.render(fullPath); 
})

// Upload image with description 
router.post('/uploadShawarmaData', adminProtectedRoute, async (req, res) => {
    // Getting the image  and changing the name 
    let image = req.files.myFile; 
    let imageName = req.files.myFile.name; 

    // Adding data to the image 
    imageName = Date().split("GMT")[0] + imageName; 
    imageName = String(imageName); 

    // Setting the path to the image 
    const imageFullPath = path.join(rootPath, 'static', 'imageData', imageName); 

    // Moving the uploaded image to the specified path 
    image.mv(imageFullPath, async (error) => {
        if (error) {
            // Specify the context type header as 'applicatio/json' file form 
            res.writeHead(500, { 'Content-Type': 'application/json'})

            // Creating a success message 
            let successMessage = JSON.stringify({ status: 'error', 'message': 'Failed to upload image file'}); 

            // Sending the error back to the client 
            return res.end(successMessage); 
        }

        // Else 
        else {
            // Saving the new created shawarma collection
            let uploadedShawarmaCollection = new PRODUCTS({
                Name: req.body.name, 
                Description: req.body.description, 
                ImageUrl: imageFullPath, 
                Price: req.body.price, 
                idValue: "null", 
                paymentVerified: false, 
            }); 

            // Saving the uploaded shawarma collection 
            try {
                // Saving the uploaded shawarma collection 
                await uploadedShawarmaCollection.save(); 

                // Create a success message, and send it back to the client 
                let successMessage = JSON.stringify({
                    "message": "Shawarma collection saved on the database", 
                    "status": "success", 
                    "statusCode": 200, 
                }); 

                // Sending back the success message 
                return res.send(successMessage).status(200); 
            }

            // Catch the error 
            catch (error) {
                // On error generated 
                let errorMessage = JSON.stringify({
                    "message": error.toString().trim(),  
                    "status": "error", 
                    "statusCode": 500, 
                }); 

                // Sending the error message 
                return res.send(errorMessage).status(500); 
            }
            
        }
    })
})


// Exporting the router 
module.exports = router; 
