
// Importing the necessary modules 
const express = require("express"); 
const protectedRoute = require('../auth/auth'); 
const shuffleArray = require('../algorithms/algorithms'); 
const { PRODUCTS } = require('../model/model'); 

// Creating the router object 
const router = express.Router(); 

// Creating a post route for creating the products 
router.post('/createShawarmaData', async(req,res) => {
    // Using try catch block to create the shawama product 
    try{
        // Saving the new shawarma data 
        const newShawarma = PRODUCTS({
            Name: req.body.name, 
            Description: req.body.description, 
            ImageUrl: req.body.imageUrl, 
            Price: req.body.price,
            paymentVerified: "false"
        })

        // Saving the new shawarma data 
        await newShawarma.save(); 

        // Generating the success message
        let successMessage = JSON.stringify({
            "message": "Shawarma data saved", 
            "statuCode": 200, 
            "status": "success"
        })

        // Return the sucess message 
        return res.send(successMessage); 
    }

    // catch the error 
    catch(error) {
        // Getting the error message 
        let errorMessage = JSON.stringify({
            "message": error.toString().trim(), 
            "status": "error"
        }); 

        // Sending back the error message 
        return res.send(errorMessage); 
    }
})

// Creating a post route for getting the shawarma data 
router.post("/getShawarmaData", protectedRoute, async (req, res) => {
    // Using try catch block to get the user's data 
    try {
        // Getting all the sharma data 
        let products = await PRODUCTS.find(); 

        // -->> Using the Algorithm to shuffle the data
        products = shuffleArray(products); 

        // Creating the data body 
        const responseData = JSON.stringify({
            "status": "success", 
            "dataArray": products, 
            "statusCode": 200, 
        })

        // Sending back the products 
        return res.send(responseData); 
    }

    // Catch the error 
    catch (error) {
        // Creating the error message 
        let errorMessage = JSON.stringify({
            "status": "error", 
            "message": error.toString().trim(), 
            "statusCode": 404, 
        }); 

        // Sending the error message 
        return res.send(errorMessage); 

    }
   
})


// Exporting the modules 
module.exports = router; 
