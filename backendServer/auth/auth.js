// Importing the necessary modules 
const jwt = require('jsonwebtoken'); 


// Creating a middle ware for checking the user is 
// logged in from the server side 
// REASONS: We don't want free access to backend the nodeJS server. 
// I think another layer of encryption might be needed in the future 
const protectedRoute = async (req, res, next) => {
    // Getting the users x-auth-token and verify before accessing the 
    // route 
    try {
        // Getting the auth-token from the header 
        const tokenHeader = req.header("x-auth-token"); 

        // Getting the jwt key 
        const jwtKey = process.env.jwtKey; 
        
        // Verify the jsownweb token 
        let isMatched = jwt.decode(tokenHeader, jwtKey); 

        // If the user's is logged in 
        if (isMatched.isLoggedIn) {
            next(); 
        }

        // Else 
        else {
            // Create the error message 
            let errorMessage = JSON.stringify({
                "status": "error", 
                "body": "User not logged in", 
                "statusCode": 401, 
            }); 

            // Sending back the message 
            return res.send(errorMessage); 
        }
    }

    // Catching error 
    catch (error) {
        // Create an error message 
        let errorMessage = JSON.stringify({
            "status": "error", 
            "body": error.toString().trim(), 
            "statusCode": 500, 
        }); 

        // Sending back the error message 
        return res.send(errorMessage); 
    }
}




// Exporting the protected route 
module.exports = protectedRoute; 