// Creating a middleware function for checking the admin 
// logged in users 
const adminProtectedRoute = (req, res, next) => {
    // Getting the value for the user's authentication, and if it's valid
    try {
        // Checking the req auth value 
        let isAuth = req.session.isAuth; 

        // Checking if the user is a valid user 
        if (isAuth) {
            // Move on to the next middleware 
            next(); 
        }

        // If the user is not authenticated 
        else {
            // Redirect the user back to the login page 
            return res.redirect('/'); 
        }
    }

    // Catch the error 
    catch (error) {
        // Redirect the user backt to the login page 
        return res.redirect('/'); 
    }
}


// Exporting the admin protected route
module.exports = adminProtectedRoute; 