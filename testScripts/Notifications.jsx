// Creating a function for push notifications 
let showNotification = (event) => {
    // Check if the browser supports the Notification API 
    if ('Notification' in window) {
        // Check if the user has granted permission for notifications 
        if (Notification.permission === 'granted') {
            // Create a new notification 
            const notification = new Notification('Hello, World!', {
                body: 'This is a sample notification message.', 
                icon: null, 
            }); 

            // Handle click events on the notification 
            notification.onclick = () => {
                alert('Notification clicked'); 
            }; 
        }

        // Else if 
        else if (Notification.permission === "denied") {
            // Handle the case where the user has denied permission for notifications 
            console.warn('Notification permission denied'); 
        } 

        // else 
        else {
            // Request notification permission from the user 
            Notification.requestPermission().then((permission) => {
                if (permission === 'granted') {
                    // The user granted permission, so show the notification
                    showNotification();
                  } else {
                    // The user denied permission
                    console.warn('Notification permission denied');
                }
            }); 
        }
    }
}