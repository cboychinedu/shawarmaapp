// Importing the necessary modules 
import React, { Component, Fragment } from 'react'; 
import Navbar from './Navbar';

// Creating a class based component for AddDebitCard 
class AddDebitCard extends Component {
    // Setting the state 
    state = {} 

    // Rendering the component 
    render() {
        // Returning the component 
        return(
            <Fragment> 
                {/* Adding the main dashboard div */}
                <section className="dashboardMainDiv"> 
                    {/* Adding the navbar */}
                    <Navbar /> 
                </section>
               

                <p> Add Debit Card  </p>
            </Fragment>
        )
    }
}

// Exporting the AddDebit card component 
export default AddDebitCard; 