// Importing the necessary modules
import React, { Component, Fragment } from 'react';
import axios from 'axios';
import "../../Styles/Dashboard.css";
import Navbar from './Navbar';
import CompanyDisplayComponent from './CompanyDisplay';
import SearchBar from './SearchBarComponent';
import { AuthContext } from '../../Auth/AuthContext';
import TrendingViewsComponent from './TrendingViewsComponent';
import ShawarmaDisplayComponent from './ShawarmaDisplayComponent';

// Creating the class based component
class Dashboard extends Component {
    // Setting the state
    state = {
        innerWidth: null,
        fullname: "", 
        cartItems: [],
    }

    // Getting the Auth context
    static contextType = AuthContext;

    // Component did mount
    componentDidMount() {
        // Getting the screen inner width
        const innerWidth = window.innerWidth;

        // Setting the state
        this.setState({
            innerWidth: innerWidth
        })

        // Make a request to the backend server to retrive the user's details
        // Getting the x-auth token if present 
        // Creating a body to hold the token value 
        const tokenBody = JSON.stringify({
            tokenValue: localStorage.getItem('x-auth-token'), 
        })

        // Getting the token value 
        const tokenValue = localStorage.getItem('x-auth-token'); 

        // Adding configurations 
        const config = {
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers': 'Content-Type', 
                'x-auth-token': tokenValue, 
            }
        }

        

        // Setting the remote server ip address 
        const serverIpAddress = `http://${process.env.REACT_APP_API_SERVER_ADDRESS}:3001/dashboard/getusers`; 

        // Making a post request to get the user's data 
        axios.post(serverIpAddress, tokenBody, config) 
        .then((responseData) => {
             
            // if the status is success 
            if (responseData.data.status === "success") {
                // Setting the state 
                this.setState({
                    fullname: responseData.data.body[0].fullname, 
                })
            }

            // Else connecting to the database had issues, 
            else {
                // Setting the state 
                this.setState({
                    fullname: "User not logged in"
                })
            }
        })



    }

    // Rendering the dashboard component
    render() {
        // Jsx for mobile devices
        if (this.state.innerWidth <= 1000) {
            // Return the jsx component for the mobile
            return(
                <Fragment>
                    <section className="dashboardMainDiv">
                        {/* Adding the navbar */}
                        <Navbar cartItems={this.state.cartItems} state={this.state}/>

                        {/* Adding the greetings message */}
                        <div className="greetingsMessageDiv">
                            <p> Hey {this.state.fullname}, <span className="boldWelcome"> Welcome </span> </p>
                        </div>

                        {/* Adding the search bar */}
                        <SearchBar />

                        {/* Adding the categories div */}
                        <div className="categoriesDiv">
                            <p className="categories"> All Categories </p>
                            <p className="seeAllPara"> See All <b> > </b> </p>

                        </div>

                        {/* Adding the trending views */}
                        <TrendingViewsComponent />

                        {/* Adding the company display component */}
                        <CompanyDisplayComponent  name="Mbonu Chinedum" />

                        {/* Adding the sharwama div Component */}
                        <ShawarmaDisplayComponent  />


                    </section>
                </Fragment>
            )
        }
    }
}

// Exporting the dashboard page
export default Dashboard;
