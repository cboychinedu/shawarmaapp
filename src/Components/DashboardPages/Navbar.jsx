// Importing the necessary modules 
import React, { Component, Fragment } from 'react'; 
import { Link } from 'react-router-dom';
import axios from 'axios';
import { AuthContext } from '../../Auth/AuthContext';
import menuLogo from "../../Assets/Images/Dashboard/menuLogo.png";
import cartLogo from '../../Assets/Images/Dashboard/cartLogo.png'; 
import closeLogo from '../../Assets/Images/Dashboard/closeLogo.png'; 
import companyLogo from '../../Assets/Images/Home/homeLogo.jpg'; 
import dashboardMenuLogo from '../../Assets/Images/Dashboard/dashboardMenuLogo.png'; 
import trackOrdersLogo from '../../Assets/Images/Dashboard/trackOrderLogo.png'; 
import profileLogo from '../../Assets/Images/Dashboard/profileLogo.png'; 
import settingLogo from '../../Assets/Images/Dashboard/settingsLogo.png'; 
import logoutLogo from '../../Assets/Images/Dashboard/logoutLogo.png'; 
import paymentLogo from '../../Assets/Images/Dashboard/paymentLogo.png'; 
import CartContainer from './CartContainer';
import '../../Styles/Navbar.css';  

// Creating the class based component 
class Navbar extends Component {
    // Setting the state 
    state = {} 

    // Getting the auth context 
    static contextType = AuthContext; 

    // Creating a function for logging out the user 
    logoutUser = (event) => {
        // Getting the context 
        const { removeToken } = this.context; 

        // Clearing the local storage 
        localStorage.clear(); 

        // Redirecting the user to the login page 
        setInterval(() => {
            window.location.href = '/login'; 
        }, 1000); 
        
    }

    // Rendering 
    render() {
        // Getting the cart items 
        let { cartItems } = this.context; 

        // Return the jsx 
        return(
            <Fragment> 
                <div className="outerMenuContainer">
                    {/* ADding the nav main div */}
                    <section className="navMainDiv">
                        {/* Adding the menu button */}
                        <button className="menuBtn" onClick={(event) => {
                            // Getting the dom element for the menuContainer 
                            const menuContainer = document.getElementById("menuContainer"); 
                            menuContainer.classList.add('open'); 
                        }}> 
                            <img src={menuLogo} className="menuLogo" /> 
                        </button>


                        {/* Adding the cart button */}
                        <button className="menuBtn cartLogoDiv" id="cartLogoDiv" onClick={(event) => {
                            // Making a post request to the back end server to save the 
                            // cartItems, and generate a token url:id /createCart/encryptedTokenvalue 
                            // console.log(cartItems); 
                            // console.log(cartIdItems); 

                            // Sending request to the backend server 
                            // Get all the user's data 
                            let userData = JSON.stringify({
                                cartItems: cartItems
                            }); 

                            // Getting the token value 
                            const tokenValue = localStorage.getItem('x-auth-token'); 

                            // Setting the axios header config 
                            const config = {
                                headers: {
                                    'Content-Type': 'application/json; charset=UTF-8',
                                    'Access-Control-Allow-Origin': '*',
                                    'Access-Control-Allow-Methods': 'POST',
                                    'Access-Control-Allow-Headers': 'Content-Type',
                                    "x-auth-token": tokenValue,  
                                }
                            }; 

                            // Setting the remote server ip address url 
                            const serverIpAddress = `http://${process.env.REACT_APP_API_SERVER_ADDRESS}:3001/cart/createCart`; 

                            // Making a post request to the serverIpAddress 
                            axios.post(serverIpAddress, userData, config) 
                            .then((responseData) => {
                                console.log(responseData); 
                                // If the cart was created 
                                if (responseData.data.status === "success") {
                                    // 
                                    window.location.href = `/cartPage/${responseData.data.cartToken}`; 
                                }
                            })


                        }}> 
                            <img src={cartLogo} className="menuLogo cartLogo" /> 
                            <span className="cartCount"> {cartItems.length} </span>
                        </button>
                    </section>


                    {/* Adding the menuContainer div */}
                    <section id="menuContainer">
                        {/* Adding the menu container header div  */}
                        <div className="menuContainerHeaderDiv">
                            <div>
                                <img src={companyLogo} className="companyLogo" /> 
                            </div>
                            <button className="closeLogoBtn" onClick={(event) => {
                                const menuContainer = document.getElementById("menuContainer"); 
                                
                                // Remove the menu after it is shown
                                setTimeout(function() {
                                    menuContainer.classList.remove('open');
                                }, 400); 
                            }}> 
                                <img src={closeLogo} className="closeLogoImage" /> 
                            </button>
                            
                        </div>
                        
                        {/* Adding the menu container items div  */}
                        <div className="menuContainerItemsDiv">
                            <section className="menuContainerItemsDivInnerUpperSection">
                                <ul> 
                                    <li className="menuContainerList"> 
                                        <Link to="/dashboard" >
                                            <img src={dashboardMenuLogo} className="menuContainerLogo" />
                                            <span className="menuHeaders"> Dashboard </span> 
                                        </Link>
                                    </li>
                                    <li className="menuContainerList"> 
                                        <Link to="/trackOrder" >
                                            <img src={trackOrdersLogo} className="menuContainerLogo" />
                                            <span className="menuHeaders"> Track Order's </span> 
                                        </Link>
                                    </li>
                                    <li className="menuContainerList"> 
                                        <Link to="/profile" >
                                            <img src={profileLogo} className="menuContainerLogo" />
                                            <span className="menuHeaders"> Profile </span> 
                                        </Link>
                                    </li>
                                    <li className="menuContainerList"> 
                                        <Link to="/addDebitCard" >
                                            <img src={paymentLogo} className="menuContainerLogo" />
                                            <span className="menuHeaders"> Add Debit Card </span> 
                                        </Link>
                                    </li>
                                    
                                </ul>

                            </section>

                            <section className="menuContainerItemsDivInnerLowerSection">
                                <ul> 
                                    <li className="menuContainerList"> 
                                        {/* <Link to="/setting" >
                                            <img src={settingLogo} className="menuContainerLogo" />
                                            <span className="menuHeaders"> Setting </span> 
                                        </Link> */}
                                    </li>
                                    <li className="menuContainerList"> 
                                        <Link to="#" onClick={this.logoutUser}>
                                            <img src={logoutLogo} className="menuContainerLogo" />
                                            <span className="menuHeaders"> Logout </span> 
                                        </Link>
                                    </li>
                                </ul>

                            </section>


                        </div>
                        
                    </section>
                   

                </div>
                {/* <button onClick={this.logoutUser}> Logout </button> */}

            </Fragment>
        )
    }
}

// exporting 
export default Navbar; 