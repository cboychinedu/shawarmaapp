const productsItems = [
    {
        "Name": "The Deluxe Shawarma", 
        "Description": "A Shawarma that speaks for itself, comes in different flavors for the bread.", 
        "ImageUrl": require('./ProductsImages/img1.png'), 
        "Price": "₦ 3,000", 
        "id": "1", 
    }, 
    {
        "Name":"Milkshakes", 
        "Description": "Different types as shown on the Menu", 
        "ImageUrl": require('./ProductsImages/img2.png'), 
        "Price": "1500", 
        "Currency": "₦", 
        "id": "2", 
    }, 
    {
        "Name": "Milkshakes", 
        "Description": "Different types as shown on the Menu", 
        "ImageUrl": require('./ProductsImages/img3.jpeg'), 
        "Price": "1500", 
        "Currency": "₦", 
        "id": "3",
    }, 
    {
        "Name": "Meshai", 
        "Description": "it comes in different sizes and is known for its mouth-watering effect", 
        "ImageUrl": require('./ProductsImages/img4.png'), 
        "Price": "3000", 
        "Currency": "₦", 
        "id": "4", 
    }, 
    {
        "Name": "Meshai", 
        "Description": "it comes in different sizes and is known for its mouth-watering effect", 
        "ImageUrl": require('./ProductsImages/img5.png'), 
        "Price": "3000", 
        "Currency": "₦", 
        "id": "5", 
    }, 
    {
       "Name": "Chicken and Chips", 
       "Description": "it comes in different sizes and is known for its mouth-watering effect", 
       "ImageUrl": require('./ProductsImages/img7.jpeg'), 
       "Price": "3000", 
       "Currency": "₦", 
       "id": "6", 
    }, 
    {
        "Name": "Chicken and Chips", 
        "Description": "it comes in different sizes and is known for its mouth-watering effect", 
        "ImageUrl": require('./ProductsImages/img8.jpeg'), 
        "Price": "3000", 
        "Currency": "₦", 
        "id": "7", 
    },
    {
        "Name": "Euphoria(Chicken)Burger", 
        "Description": "Also comes with Chips", 
        "ImageUrl": require('./ProductsImages/img9.jpeg'), 
        "Price": "3000", 
        "Currency": "₦", 
        "id": "8"
    }, 
    {
        "Name": "Ecstasy(Beef) Burger", 
        "Description": "Comes with a side of frides and ketchup and it's Crispy Crunchy and Completely sensational", 
        "ImageUrl": require('./ProductsImages/img10.jpeg'), 
        "Price": "3000", 
        "Currency": "₦", 
        "id": "9"
    }, 
    {
        "Name": "Ecstasy(Beef) Burger", 
        "Description": "Comes with a side of frides and ketchup and it's Crispy Crunchy and Completely sensational", 
        "ImageUrl": require('./ProductsImages/img10.jpeg'), 
        "Price": "3000", 
        "Currency": "₦", 
        "id": "10"
    }, 
    {
        "Name": "Regular Shawarma", 
        "Description": "Hungry for something Extraordinary? Then you're in the right place, also comes in different flavors for the bread", 
        "ImageUrl": require('./ProductsImages/img14.png'), 
        "Price": "2200", 
        "Currency": "₦", 
        "id": "11"
    }, 
    {
        "Name": "Noodles", 
        "Description": "Comes with sides and Protein of your choice and could also vary, from Shredded beef to hot and spicy wings or Turkey", 
        "ImageUrl": require('./ProductsImages/img15.jpeg'), 
        "Price": "2200", 
        "Currency": "N", 
        "id": "12"
    }, 
    {
        "Name": "Noodles", 
        "Description": "Comes with sides and Protein of your choice and could also vary, from Shredded beef to hot and spicy wings or Turkey", 
        "ImageUrl": require('./ProductsImages/img16.jpeg'), 
        "Price": "2200", 
        "Currency": "N", 
        "id": "13"
    }, 
    {
        "Name": "Chicken and Chips", 
        "Description": "Tasty", 
        "ImageUrl": require('./ProductsImages/img17.jpg'), 
        "Price": "3000", 
        "Currency": "N", 
        "id": "14"
    }, 
    {
        "Name": "The bun jovi(Double Patty)", 
        "Description": "The bun jovi comprises of two Patties, could be either beef or chicken or both.", 
        "ImageUrl": require("./ProductsImages/img18.jpg"), 
        "Price": "5000", 
        "Currency": "N", 
        "id": "15", 
    }, 
    {
        "Name": "Peppered Turkey",
        "Description": "Always Available... just like everything else on the Menu", 
        "ImageUrl": require('./ProductsImages/img19.jpg'), 
        "Price": "2500", 
        "Currency": "N", 
        "id": "16", 
    }, 
    {
        "Name": "Peppered Turkey",
        "Description": "Always Available... just like everything else on the Menu", 
        "ImageUrl": require('./ProductsImages/img20.jpg'), 
        "Price": "2500", 
        "Currency": "N", 
        "id": "17", 
    }, 
    {
        "Name": "Peppered Turkey",
        "Description": "Always Available... just like everything else on the Menu", 
        "ImageUrl": require('./ProductsImages/img21.jpg'), 
        "Price": "2500", 
        "Currency": "N", 
        "id": "18", 
    }, 
    {
        "Name": "Peppered Chicken", 
        "Description": "Tender and luscious", 
        "ImageUrl": require('./ProductsImages/img22.jpg'), 
        "Price": "2000", 
        "Currency": "N", 
        "id": "19", 
    }
]

// Exporting the products 
export default productsItems; 