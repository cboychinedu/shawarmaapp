// Importing the necessary moddules 
import React, { Component, Fragment } from 'react'; 
import searchLogo from '../../Assets/Images/Dashboard/searchLogo.png'; 
import axios from 'axios';
import '../../Styles/Dashboard.css'; 

// Creating a class based component for the search bar 
class SearchBar extends Component {
    // Rendering the search bar component 
    render() {
        // Returning the search div 
        return(
            <Fragment> 
                <div className="searchDiv"> 
                    <div>
                        <img src={searchLogo} className="searchLogo" /> 
                    </div>
                    <div>
                        <input className="searchInputForm" type="text" placeholder='Search dishes, shawarma' /> 
                    </div>
                </div>
            </Fragment>
        )
    }
}

// Exporting the searchBarComponent 
export default SearchBar; 