// Importing the necessary modules 
import React, { Component, Fragment } from 'react'; 
import Navbar from './Navbar';
import { Icon, marker } from 'leaflet';
import { AuthContext } from "../../Auth/AuthContext";
import "../../Styles/TrackOrder.css"
import { 
    MapContainer, 
    TileLayer, 
    Popup, 
    Circle, 
    Marker, 
    CircleMarker
} from 'react-leaflet'; 
import markerIcon from '../../Assets/Images/Maps/marker-icon.png'; 

// Creating the marker icon 
const customIcon = new Icon({
    iconUrl: markerIcon, 
    iconSize: [38, 38]
}); 

// Creating a class based component for tracking the order 
class TrackOrder extends Component {
    // Setting the state 
    state = {
        latitude: 6.5479775, 
        longitude: 3.0037633, 
    } 

    // Component did mount 
    componentDidMount() {
        // Getting the user's location 
        window.navigator.geolocation.getCurrentPosition((position) => {
            // Setting the state 
            this.setState({
                latitude: position.coords.latitude, 
                longitude: position.coords.longitude
            })
        })
    }

    // Getting the Auth context 
    static contextType = AuthContext; 

    // Rendering the component 
    render() {
        // Returning the componnet 
        return(
            <Fragment> 
                {/* Adding the main dashboard div */}
                <section className="trackOrderMainDiv"> 
                    {/* Adding the back button */}
                    <div className="trackOrderButtonDiv"> 
                        <button className="goBackBtn" onClick={(event) => {
                            // Setting the time out, then redirect the user to the dashboard page 
                            setTimeout(() => {
                                // Redirecting the user to the dashboard page  
                                window.location.href = '/dashboard'; 
                            }, 1000)

                        }}>  Go Back </button>
                    </div>
                    

                    {/* Adding the map  */}
                    <div className="mapDiv"> 
                        <MapContainer center={[this.state.latitude, this.state.longitude]} zoom={10} scrollWheelZoom={false} > 
                            <TileLayer attribution='Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                                    url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png' />

                            {/* Adding the default Marker */}
                                <Marker position={[this.state.latitude, this.state.longitude]} icon={customIcon}>
                                    <Popup>
                                        <p> This is your current location. </p>
                                    </Popup>
                            </Marker>
                        
                        </MapContainer>
                    </div>
                   
                </section>

            </Fragment>
        )
    }
}

// Exporting the track order component 
export default TrackOrder; 