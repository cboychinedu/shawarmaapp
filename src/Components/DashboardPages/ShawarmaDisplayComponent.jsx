// Importing the necessary modules 
import React, { Component, Fragment } from 'react'; 
import addLogo from '../../Assets/Images/Dashboard/addLogo.png'; 
import { AuthContext } from '../../Auth/AuthContext';
import axios from 'axios';

// Creating the component 
class ShawarmaDisplayComponent extends Component { 
    // Getting the Auth context 
    static contextType = AuthContext; 

    // Setting the state 
    state = {
        productsItems: []
    }

    // Componenet did mount 
    componentDidMount() {
        // Making a fetch request to get all the shawarma data, but 
        // first get the token value from the local storage
        const tokenValue = localStorage.getItem('x-auth-token'); 

        // Setting the axios headers 
        const config = {
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers': 'Content-Type',
                "x-auth-token": tokenValue, 
            }
        }

        // Setting the body 
        const bodyMessage = JSON.stringify({
            data: "getShawarmaData"
        })

        // Setting the remote server ip address 
        const serverIpAddress = `http://${process.env.REACT_APP_API_SERVER_ADDRESS}:3001/products/getShawarmaData`;

        // Making a post request to get the products data 
        axios.post(serverIpAddress, bodyMessage, config)
        .then((responseData) => {

            // if the status is success 
            if (responseData.data.status === "success") {
                // Setting the state 
                this.setState({
                    productsItems: responseData.data.dataArray
                })
            }

            // else 
            else if (responseData.data.status === "error") {
                // Setting the state 
                console.log(responseData.data.body); 
            }

        })
    }

    // Render the compnent 
    render() {
        // 
        let { cartItems, addCartItems } = this.context; 
        // console.log(this.state.productsItems); 

        // Return the jsx component 
        return(
            <Fragment> 
               {/* Adding the header div */}
                <div className="shawarmaHeaderDiv">
                    <h1> Shawarma ({this.state.productsItems.length})</h1>
                </div>

                {/* Adding the products */}
                <section className="gridContainer">
                    { 
                        this.state.productsItems.map((product) => {
                            // Getting the image name, and create a url 
                            let imageName = product.ImageUrl.split("/imageData/")[1]; 
                            const imageUrl = `http://${process.env.REACT_APP_API_SERVER_ADDRESS}:3001/images/${imageName}`;
                            
                            // Rendering the data 
                            return(
                                <div className="gridItem" id={product._id} key={product._id}>
                                    <div>
                                        <img src={imageUrl} className="trendingImageViews" />
                                    </div>
                                    <div className="mainPriceDescriptionDiv">
                                        <div className="priceDescriptionDiv">
                                            <p className="bold"> {product.Name} </p>
                                            <p className="productDesc"> {product.Description} </p>
                                        </div>
                                        <div className="priceDiv">
                                            <section>
                                                <p className="bold"> NGN {product.Price} </p>
                                            </section>
                                            <button style={{border: "none"}}>
                                                <img src={addLogo} alt="" className="addLogo" onClick={addCartItems} id={product._id} key={product._id}/>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                // <div className="gridItem" onClick={this.props.addItemToCart}>
                                //     <img src={element.ImageUrl} className="trendingImageViews"/> 
                                // </div>
                            )
                        })
                    }
                    
                    {/* <div className='gridItem' onClick={this.props.addItemToCart}>
                        <p> Test </p>
                    </div> */}


                </section>

            </Fragment>
        )
    }
}

// Exporting the ShawarmaDisplayComponent 
export default ShawarmaDisplayComponent;