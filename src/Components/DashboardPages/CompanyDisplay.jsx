/* eslint-disable jsx-a11y/alt-text */
// Importing the necessary modules 
import React, { Fragment } from 'react'; 
import imageLogo from "../../Assets/Images/Home/homeLogo2.jpg"; 
import starLogo from '../../Assets/Images/Dashboard/starLogo.png'; 
import busLogo from '../../Assets/Images/Dashboard/busLogo.png'; 
import timeLogo from '../../Assets/Images/Dashboard/timeLogo.png'; 


// Creating the componay display component 
const CompanyDisplayComponent = (props) => {

    // Rendering the component 
    return(
        <Fragment>
            <div className="companyDisplayMainDiv">
                {/* Adding the image div */}
                <section className="companyDisplayImageDiv"> 
                    <img src={imageLogo} className="companyDisplayImageLogo"/> 
                </section>

                {/* Addding the description div */}
                <section className="companyDisplayDescriptionDiv">
                    {/* Adding the header div */}
                    <div>
                        <h1> Ecstasy Softbite </h1>
                    </div>
                    {/* Adding the description div */}
                    <div>
                        <p className="companyDisplayPara"> 
                            The best of the best when it comes to <b> Shawarma</b>, Grills, Intercontinental Dishes, Pasteries, Seafood, 
                            Pasta and Much more in Awka, Anambra State. <br /> 
                            You satisfaction is our Goal, so feel free to place an Order and allow us to provide you comfort with our foods. 
                        </p>
                    </div>
                    {/* Adding the company rating div */}
                    <div className="companyDisplayRatingMainDiv">
                        <section>
                            <div className="innerDisplayRatingDiv">
                                <img src={starLogo} className="innerDisplayRatingLogo"/> 
                                <b className="ratingPara"> 4.78 </b>
                            </div>
                        </section>
                        <section>
                            <div className="innerDisplayRatingDiv">
                                <img src={busLogo} className="innerDisplayRatingLogo" /> 
                                <b className="ratingPara"> Delivery </b>
                            </div>
                        </section>
                        <section>
                            <div className="innerDisplayRatingDiv">
                                <img src={timeLogo} className="innerDisplayRatingLogo"/> 
                                <b className="ratingPara"> 20 mins </b>
                            </div>
                        </section>
                    </div>

                    {/* Adding the products selection div */}
                    <div className="companyDisplayProductsSelectionDiv">
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> The Delux Shawarma</button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Regular Shawarma</button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Tarzan's Meaty Feast </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> The bun jovi (Double Patty) </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Euphoria (Chicken) Burger </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Ecstasy(Beef) Burger </button>
                        </div>

                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Chicken and Chips </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Turkey and Chips </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Noodles </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Pasta </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Peppered Turkey </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Hot and Spicy Wings </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Peppered Chicken </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Mini Meshai </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Meshai </button>
                        </div>
                        <div className="companyDisplaySelections">
                            <button className="companyDisplaySelectionBtn"> Milkshakes </button>
                        </div>

                    </div>
                    

                   

                </section>

            </div> 

        </Fragment>
    )
}

// Exporting the Company display component 
export default CompanyDisplayComponent; 