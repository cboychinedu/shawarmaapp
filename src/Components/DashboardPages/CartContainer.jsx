/* eslint-disable jsx-a11y/alt-text */
// Importing the necessary modules 
import React, { Component, Fragment } from 'react'; 
import companyLogo from '../../Assets/Images/Home/homeLogo.jpg'; 
import { AuthContext } from '../../Auth/AuthContext';
import shwamaLogo from './Products/ProductsImages/img1.jpg';
import removeLogo from '../../Assets/Images/Dashboard/removeLogo.png'; 
import PaystackPaymentComponent from './PaystackPayment'; 
import axios from 'axios';
import "../../Styles/Cart.css"; 

// Creating the class based component 
class CartContainer extends Component {
    // Setting the state 
    state = {
        cartItems: [], 
        cartTotalPrice: null, 
        emaiAddress: "", 
    }

    // Getting the auth context 
    static contextType = AuthContext; 

    // Creating a component did mount function 
    componentDidMount() {
        // Getting the token value 
        const tokenValue = localStorage.getItem('x-auth-token');  

        // Making a connection to the backend to retrive the token data 
        const bodyMessage = JSON.stringify({
            "tokenValue": window.location.href.split("/cartPage/")[1]
        }); 

        // Setting the axios headers configuration 
        const config = {
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers': 'Content-Type',
                "x-auth-token": tokenValue, 
            }
        }

        // Setting the remote server ip address url
        let serverIpAddress = `http://${process.env.REACT_APP_API_SERVER_ADDRESS}:3001/cart/getCart`;

        // Making an axios request 
        axios.post(serverIpAddress, bodyMessage, config) 
        .then((responseData) => {
            // Setting the state 
            this.setState({
                cartItems: responseData.data.items, 
                cartTotalPrice: responseData.data.totalFee, 
            })
        })

        // Making an axios request to get the user's email address 
        // Setting the remote server ip address for getting the user's email address 
        serverIpAddress = `http://${process.env.REACT_APP_API_SERVER_ADDRESS}:3001/dashboard/getusers`;
        
        // Creating a body to hold the token value 
        const tokenBody = JSON.stringify({
            tokenValue: localStorage.getItem('x-auth-token'), 
        })

        // Making a post request to get the user's data 
        axios.post(serverIpAddress, tokenBody, config)
        .then((response) => {
            // If the status is success 
            if (response.data.status === "success") {
                // Setting the state 
                this.setState({
                    emaiAddress: response.data.body[0].emailAddress 
                })
            }
        })
    }

    // Creating a function for removing the cart product 
    removeProduct = (event) => {
        // Getting the product id value 
        const itemId = event.target.id; 

        // Getting the token value 
        const tokenValue = localStorage.getItem('x-auth-token'); 

        // Making a connection to the backend to retrive the token data 
        const bodyMessage = JSON.stringify({
            "cartToken": window.location.href.split("/cartPage/")[1], 
            "itemIdToDelete": itemId, 
        }); 

        // Setting the axios headers configuration 
        const config = {
            headers: {
                'Content-Type': 'application/json; charset=UTF-8',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers': 'Content-Type',
                "x-auth-token": tokenValue, 
            }
        }

        // Setting the remote server ip address for getting the user's email address 
        let serverIpAddress = `http://${process.env.REACT_APP_API_SERVER_ADDRESS}:3001/cart/deleteCartItem`;

        // Making a post request to get the user's data 
        axios.post(serverIpAddress, bodyMessage, config)
        .then((response) => {
            // If the status is success 
            if (response.data.status === "success") {
                // Reload the cart page 
                setTimeout(() => {
                    window.location.reload(); 
                }, 1000); 

            }
        })

    }

    // Rendering the component 
    render() {
        // Getting the cart items 
        const { cartItems } = this.state;

        // Setting the config for paystack 
        const config = {
            reference: (new Date()).getTime().toString(),
            email: this.state.emaiAddress,
            currency: "NGN", 
            amount: Number.parseFloat(this.state.cartTotalPrice) * 100, 
            publicKey: process.env.REACT_APP_PUBLIC_KEY,
        }
        
        // Return the component 
        return(
            <Fragment> 
                {/* Adding the menu container header div  */}
                <div className="menuContainerHeaderDiv">
                    <div>
                        <img src={companyLogo} className="companyLogo" alt="companyLogo" aria-label='this is a company logo'/> 
                        <p className="myOrderPara" aria-label='my order paragraph'> My order </p>
                    </div>

                    {/* <p> {cartItems.length}</p> */}
                    
                </div>

                {/* Displaying all the selected carts */}
                <div className="cartMainDiv"> 
                    {/* <button onClick={this.viewAllSelectedCart}> TEst </button> */}=
                    {/* { console.log(this.props.state.cartItems)} */}
                    { (this.state.cartItems) ? (
                        <> 
                            {cartItems.map((items) => {
                                // Getting the image name, and create a url 
                                let imageName = items.ImageUrl.split("/imageData/")[1]; 
                                const imageUrl = `http://${process.env.REACT_APP_API_SERVER_ADDRESS}:3001/images/${imageName}`;

                                // Test 
                                // console.log(items); 

                                // Rendering the component 
                                return(
                                    <Fragment> 
                                        <div className="cartInnerMainDiv" key={items._id}>
                                            <div>
                                                <img src={imageUrl} className="productImage" /> 
                                            </div>
                                            <div className="cartDescriptionDiv">
                                                <div className="innerCartDecriptionDiv">
                                                    <p className="cartNamePara cartBold"> {items.Name} </p>
                                                    <p className="cartDescriptionPara"> {items.Description} </p>
                                                    <b className="cartPricePara"> NGN {items.Price} </b>
                                                </div>
                                            </div>
                                            <div>
                                                <button className="cartRemoveBtn"> 
                                                    <img src={removeLogo} className="cartRemoveBtnLogo" id={items._id} onClick={this.removeProduct} /> 
                                                </button>
                                            </div>
                                        </div>
                                        
                                    </Fragment>
                                )
                            })}
                        </>
                        ) :
                        (
                            <Fragment> 
                                <p> No cart item go back to select an item... </p>
                            </Fragment>
                        )
                    }
                </div>

                <div className="cartCheckOutSection">
                    <div className="checkoutFirstDiv">
                        <p className="cartPricePara"> Price: </p>
                        <p className="cartPriceParaValue"> N {this.state.cartTotalPrice} </p>
                    </div>
                    <div className="checkoutFirstDiv">
                        <p className="cartPricePara"> Total Fee </p>
                        <p className="cartPriceParaValue"> N {this.state.cartTotalPrice}</p>
                    </div>

                    <div className="cartCheckoutButtonDiv">
                        <PaystackPaymentComponent className="checkoutbtn" config={config} />
                        {/* <button className="checkoutBtn"> Checkout </button> */}
                    </div>
                </div>

            </Fragment>
        )
    }
}

// "Name": "Peppered Turkey", 
// "Description": "Always available, just like everything else on the menu", 
// "ImageUrl": require('./ProductsImages/img1.jpg'), 
// "Price": "₦ 2,500", 
// "id": "0", 

// Export 
export default CartContainer; 