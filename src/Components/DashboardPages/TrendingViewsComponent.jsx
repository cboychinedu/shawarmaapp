// Importing the necessary modules 
import React, { Component, Fragment } from 'react'; 
import testImage from '../../Assets/Images/Dashboard/testImage.jpg';
import pepperedChickedLogo from '../DashboardPages/Products/ProductsImages/img22.jpg'; 
import milkshakesLogo from '../DashboardPages/Products/ProductsImages/img3.jpeg'; 
import meshaiLogo from '../DashboardPages/Products/ProductsImages/img4.png'; 
import chickenAndChipsLogo from '../DashboardPages/Products/ProductsImages/img7.jpeg'; 
import addLogo from '../../Assets/Images/Dashboard/addLogo.png'; 

// Creating a class based component 
class TrendingViewsComponent extends Component {
    // Setting the state 
    state = {
        isExpanded: false, 
    }

    // Creating a function for handling the hover click 
    handleDivClick = (event) => {
        // Set the state 
        this.setState({
            isExpanded: true
        }); 

        // Set time out 
        setTimeout(() => {
            this.setState({
                isExpanded: false
            })
        }, 4000); 
    }; 

    // Rendering the component 
    render() {
        // Returning the jsx component 
        return(
            <Fragment> 
                {/* Expanded Div */}
                {
                    this.state.isExpanded ? (
                        <p> Image Display </p>
                    ): null
                }
                {/* Adding the side slide */}
                <div className="trendingViews">
                    <div className="viewOne">
                        <div>
                            <img src={pepperedChickedLogo} className="trendingImageViews" />
                        </div>
                        <div>
                            <div className="priceDescriptionDiv">
                                <p className="bold"> Peppered Chicken </p>
                                <p className="trendingViewsDescPara"> Tender and luscious </p>
                            </div>
                            <div className="priceDiv">
                                <section>
                                    <p className="bold">  ₦ 2000 </p>
                                </section>
                                {/* <section>
                                    <img src={addLogo} alt="" className="addLogo" />
                                </section> */}

                            </div>
                            
                        </div>
                    </div>
                    <div className="viewOne">
                        <div>
                            <img src={milkshakesLogo} className="trendingImageViews" />
                        </div>
                        <div>
                            <div className="priceDescriptionDiv">
                                <p className="bold"> Milkshakes </p>
                                <p className="trendingViewsDescPara"> Of different types as shown on the Menu </p>
                            </div>
                            <div className="priceDiv">
                                <section>
                                    <p className="bold"> ₦ 1500 </p>
                                </section>
                                {/* <section>
                                    <img src={addLogo} alt="" className="addLogo" />
                                </section> */}

                            </div>
                            
                        </div>
                    </div>
                    <div className="viewOne">
                        <div>
                            <img src={meshaiLogo} className="trendingImageViews" />
                        </div>
                        <div>
                            <div className="priceDescriptionDiv">
                                <p className="bold"> Meshai </p>
                                <p className="trendingViewsDescPara"> It comes in different types as shown above. </p>
                            </div>
                            <div className="priceDiv">
                                <section>
                                    <p className="bold">  ₦ 3000 </p>
                                </section>
                                {/* <section>
                                    <img src={addLogo} alt="" className="addLogo" />
                                </section> */}

                            </div>
                            
                        </div>
                    </div>
                    <div className="viewOne">
                        <div>
                            <img src={chickenAndChipsLogo} className="trendingImageViews" />
                        </div>
                        <div>
                            <div className="priceDescriptionDiv">
                                <p className="bold"> Chicken and Chips </p>
                                <p className="trendingViewsDescPara"> Spicy  </p>
                            </div>
                            <div className="priceDiv">
                                <section>
                                    <p className="bold">  ₦ 3000 </p>
                                </section>
                                {/* <section>
                                    <img src={addLogo} alt="" className="addLogo" />
                                </section> */}

                            </div>
                            
                        </div>
                    </div>
                </div>

            </Fragment>
        )
    }
}

// Exporting the trending views component 
export default TrendingViewsComponent; 