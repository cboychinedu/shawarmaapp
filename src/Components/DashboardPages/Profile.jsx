// Importing the necessary modules 
import React, { Component, Fragment } from 'react'; 
import Navbar from './Navbar';

// Creating a class based component for Profile 
class Profile extends Component {
    // Setting the state 
    state = {} 

    // Rendering the component 
    render() {
        // Returning the componnet 
        return(
            <Fragment> 
                {/* Adding the main dashboard div */}
                <section className="dashboardMainDiv"> 
                    {/* Adding the navbar */}
                    <Navbar /> 
                </section>
               

                <p> Profile Component </p>
            </Fragment>
        )
    }
}

// Exporting the track order component 
export default Profile; 