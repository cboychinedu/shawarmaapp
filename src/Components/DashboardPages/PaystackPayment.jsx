// Importing the necessary modules 
import React, { Component, Fragment } from 'react'; 
import { PaystackButton } from 'react-paystack';
import "../../Styles/Dashboard.css";
  
let PaystackPaymentComponent = (props) => {
    let config = props.config; 

    // you can call this function anything
    const handlePaystackSuccessAction = (reference) => {
      // Implementation for whatever you want to do with reference and after success call.
      console.log(reference);
    };

    // you can call this function anything
    const handlePaystackCloseAction = () => {
      // implementation for  whatever you want to do when the Paystack dialog closed.
      console.log('closed')
    }

    const componentProps = {
        ...config,
        text: 'Checkout',
        // onSuccess: (reference) => alert("Thanks for doing business with us! Come back soon!!"),
        onSuccess: (reference) => handlePaystackSuccessAction(reference),
        onClose: handlePaystackCloseAction,
    };

    return (
      <div className="payButtonDiv">
        <PaystackButton {...componentProps} className="payButton" /> 
      </div>
    );
  }
  
  export default PaystackPaymentComponent;
