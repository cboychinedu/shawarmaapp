// Creaging the items component
const items = [
    {
        headerText: "Order from choosen chef", 
        homeLogo: require('../Assets/Images/Home/homeLogo.jpg'), 
        headerPara: `Get all your foods in one place. You just place the order and we'll do the rest. `, 
        headerParaTwo: 'Please click Accept to grant permission for this feature, allowing us to provide you with a seamless and efficient ordering process.', 
    }, 
    {
        headerText: "Free delivery offers", 
        homeLogo: require('../Assets/Images/Home/homeLogo2.jpg'),
        headerPara: `Furthermore, to enhance your shawarma pre-ordering experience, 
                        we may require access to your device's geolocation information.`, 
        headerParaTwo: `Enabling the geotagging feature will ensure precise tracking and delivery of your mouthwatering shawarma.`, 
        headerParaThree: `Please click 'Accept' to grant permission for this feature, allowing us to provide you with a seamless and efficient ordering process.`, 
    }, 
    // Add more items as needed
];

// Exporting 
export default items; 