// Importing the necessary modules 
import React, { Component, Fragment } from 'react'; 

// Creating the functional component 
const LoaderComponent = (props) => {
    // Returning the component 
    return(
        <Fragment> 
            <div className="lds-roller" style={{height: props.innerHeight}}>
                <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
            </div>
        </Fragment>
    )
}

// Exporting the loaderComponent 
export default LoaderComponent