// Importing the necessary modules
import React, { Component, Fragment, useEffect } from 'react';
import { Link } from 'react-router-dom';
import sweetAlert from './SweetAlert';
import { AuthContext } from "../Auth/AuthContext";
import "../Styles/Login.css";
import axios from 'axios';
import LoaderComponent from './LoaderComponent';
import { flashMessageFunction } from './FlashMessage';

// Creating the class based component
class Login extends Component {
    // Setting the state
    state = {
        innerWidth: null,
        innerHeight: null,
        statusMessage: "",
        isLoading: false,
    }

    // Show the loader when the login button is clicked
    showLoader = () => {
        // Setting the state
        this.setState({ isLoading: true })
    }

    // Hide the loader when the dashboard page is rendered
    hideLoader = () => {
        // Setting the state
        this.setState({ isLoading: false })
    }

    // Getting the Auth context
    static contextType = AuthContext;

    // Component did mount
    componentDidMount() {
        // Getting the innerWidth
        const innerWidth = window.innerWidth;
        const innerHeight = window.innerHeight;

        // Setting the state
        this.setState({
            innerWidth: innerWidth,
            innerHeight: innerHeight,
        })
    }

    // Creating a function for removing the flash message
    removeFlashMessage = (event) => {
        // Removing the flash message
        event.target.className = "inputForm";
    }

    // Creating a function for handling the login button
    handleLogin = (event) => {
        // Preventing the default submission
        event.preventDefault();

        // Getting the dom element for the login form
        const emailAddress = document.getElementById("emailAddress");
        const password = document.getElementById("password");
        const flashMessageDiv = document.getElementById("flashMessageDiv");

        // Checking if the emailAddress is valid
        if (emailAddress.value === '') {
            // Setting the state
            this.setState({
                statusMessage: "Email address is required",
            })

            // Opening the flash message
            flashMessageFunction(flashMessageDiv, emailAddress);
        }

        // Checking if the password is valid
        else if (password.value === '') {
            // Setting the state
            this.setState({
                statusMessage: "The user password is required",
            })

            // Opening the flash message
            flashMessageFunction(flashMessageDiv, password);
        }

        // Else if all the conditions were satified, execute
        // the else block below
        else {
            // Get all the user's login data
            let userData = JSON.stringify({
                "emailAddress": emailAddress.value,
                "password": password.value
            });

            // Setting the axios headers config
            const config = {
                headers: {
                        'Content-Type': 'application/json; charset=UTF-8',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Methods': 'POST',
                        'Access-Control-Allow-Headers': 'Content-Type',
                        "x-auth-token": "null", 
                    },
            };

            // Setting the remote server ip address url
            const serverIpAddress = `http://${process.env.REACT_APP_API_SERVER_ADDRESS}:3001/login`;

            // Making a post request to the serverIpAddress
            axios.post(serverIpAddress, userData, config)
            .then((responseData) => {
                if (responseData.data.status === "success") {
                    // Getting the setToken function from the context
                    // libary
                    const { setToken } = this.context;

                    // Showing the loader
                    this.showLoader();
                    // window.location.href = '/dashboard';

                    // Displaying the login message
                    // sweetAlert(responseData.data.status, responseData.data.message, 'You are now logged in');

                    // Delay the login duration for 3 seconds
                    setTimeout(() => {
                        // Saving the x-auth-token into the localStorage memory,
                        // and state data
                        localStorage.setItem('x-auth-token', responseData.data['x-auth-token']);
                        setToken(responseData.data['x-auth-token']);

                        // Redirecting the user to the dashboard page
                        window.location.href = '/dashboard';
                        window.location.reload();
                    }, 2000);

                }

                // Else if the data from the request was an error
                else {
                    // Using sweet alert
                    // console.log(responseData)
                    sweetAlert('error', 'Invalid email or password', 'Invalid email or password');
                }
            })
        }
    }

    // Rendering the component
    render() {
        // Jsx for mobile devices
        if (this.state.innerWidth <= 1000) {
            // Return the jsx component for the mobile
            return(
                <Fragment>
                    {this.state.isLoading ? (
                        // Adding the loader component
                        <LoaderComponent innerHeight={this.state.innerHeight}/>
                    ):
                    (
                        <section className="loginMainDiv">
                            {/* Adding the Display Message Div */}
                            <div className="loginDisplayHeaderDiv">
                                <div className="flashMessageDiv" id="flashMessageDiv">
                                    <p> {this.state.statusMessage} </p>

                                </div>
                                <h1 className="loginDisplayHeader"> Log In </h1>
                                <p className="loginDisplayPara"> Please sign in to your existing account </p>
                            </div>

                            {/* Adding the login div */}
                            <div className="loginDiv">
                                <div>
                                    <label className="labelHeader"> EMAIL </label> <br />
                                    <input type="email" placeholder='Email address' className="inputForm" id="emailAddress" onClick={this.removeFlashMessage} />
                                </div>

                                <div>
                                    <label className="labelHeader"> PASSWORD </label> <br />
                                    <input type="password" placeholder='Password' className="inputForm" id="password" onClick={this.removeFlashMessage}/>
                                </div>

                                <div className="loginFormPlaceholderDiv">
                                    <div className="loginCheckboxDiv">
                                        <input type="checkbox" name="" id="" className="loginCheckbox" />
                                        <Link className="loginLinkTag" to="#"> Remember me </Link>
                                    </div>
                                    <div>
                                        <Link to="/forgotPassword" className="loginLinkTag forgotPasswordLink"> Forgot Password</Link>
                                    </div>

                                </div>

                                <div>
                                    <button className="loginBtn" onClick={this.handleLogin}> LOG IN </button>
                                </div>

                                <div className="loginDontHaveAnAccountDiv">
                                    <p> Don't have an account ? <Link to='/register' className="loginLinkTag forgotPasswordLink"> Register </Link></p>
                                </div>

                            </div>

                        </section>
                    )}

                </Fragment>
            )
        }

        // else for PC and Tablet devices
        else {
            // Returning the jsx component for PC and Tablet Devices
            return(
                <Fragment>
                    <p> PC-Login Page </p>
                </Fragment>
            )
        }

    }
}

// Exporting the Login component
export default Login;
