// Importing the necessary modules
import React, { Component, Fragment, useEffect } from 'react';
import { Link } from 'react-router-dom';
import sweetAlert from './SweetAlert';
import { flashMessageFunction } from './FlashMessage';
import "../Styles/Register.css";
import axios from 'axios';

// Creating the class based component
class Register extends Component {
    // Setting the state
    state = {
        innerWidth: null,
        status: false,
        statusMessage: "",
    }

    // Creating a function for handling the register button
    handleRegister = (event) => {
        // Getting the dom elements for the register form
        const fullname = document.getElementById("fullname");
        const emailAddress = document.getElementById("emailAddress");
        const phoneNumber = document.getElementById("phoneNumber");
        const password = document.getElementById("password");
        const passwordConfirmation = document.getElementById("passwordConfirmation");
        const flashMessageDiv = document.getElementById("flashMessageDiv");

        // Checking if the fullname is valid
        if (fullname.value === '') {
            // Setting the state
            this.setState({
                status: true,
                statusMessage: "Fullname is required",
            })

            // Opening the flash message
            flashMessageFunction(flashMessageDiv, fullname);

        }

        // Checking if the emailAddress is valid
        else if (emailAddress.value === '') {
            // Setting the state
            this.setState({
                statusMessage: "Email address is required",
            })

            // Opening the flash message
            flashMessageFunction(flashMessageDiv, emailAddress);
        }

        // Checking if the phoneNumber is valid
        else if (phoneNumber.value === '') {
            // Setting the state
            this.setState({
                statusMessage: "Phone number is required",
            })

            // Opening the flash message
            flashMessageFunction(flashMessageDiv, phoneNumber);
        }

        // Checking if the password is valid
        else if (password.value === '') {
            // Setting the state
            this.setState({
                statusMessage: "The user password is required",
            })

            // Opening the flash message
            flashMessageFunction(flashMessageDiv, password);
        }

        // Checking if the retype password is valid
        else if (passwordConfirmation.value === '') {
            // Setting the state
            this.setState({
                statusMessage: "Re-type your password",
            });

            // Opening the flash message
            flashMessageFunction(flashMessageDiv, passwordConfirmation);
        }

        // Checking if the password is correct
        else if (password.value !== passwordConfirmation.value) {
            // Setting the state
            this.setState({
                statusMessage: "Passwords not correct",
            })

            // Opening the flash message
            flashMessageFunction(flashMessageDiv, passwordConfirmation);
        }

        // Else if all the conditions were satified
        else {
            // Get all the user's data
            let userData = JSON.stringify({
                "fullname": fullname.value,
                "emailAddress": emailAddress.value,
                "phoneNumber": phoneNumber.value,
                "password": password.value,
            })

            // Setting the headers configurations
            const config = {
            headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'POST',
                    'Access-Control-Allow-Headers': 'Content-Type',
                    "x-auth-token": "null", 
                },
            };

            // Setting the remote server Ip Address URL
            const serverIpAddress = `http://${process.env.REACT_APP_API_SERVER_ADDRESS}:3001/register`;

            // Making a post request to the serverIpAddress
            axios.post(serverIpAddress, userData, config)
            .then((responseData) => {
                if (responseData.data.status === "success") {
                    // User registered on the database
                    sweetAlert("success", "User Registered", responseData.data.message);

                    // Redirecting the user to the login page
                    setTimeout(() => {
                        window.location.href = '/login';
                    }, 4000);
                }

                // Else if there was an error in registration
                else if (responseData.data.status === "user-registered-error") {
                    // Error in registration
                    sweetAlert(responseData.data.status, 'Error registering user', responseData.data.message);

                    // Redirecting the user to the login page
                    setTimeout(() => {
                        window.location.href = '/login';
                    }, 4000);

                }

                // Else if server error
                else if (responseData.data.status === "error") {
                    // Error in registration
                    sweetAlert(responseData.data.status, 'Error connecting to the database', responseData.data.message);

                    // Stopping the runtime
                    return;
                }
            })

        }

    }

    // Removing the flash message
    removeFlashMessage = (event) => {
        // Removing the flash message
        event.target.className = "inputForm";
    }

    // Component did mount
    componentDidMount() {
        // Getting the innerWidth
        const innerWidth = window.innerWidth;

        // Setting the state
        this.setState({
            innerWidth: innerWidth,
        })
    }

    // Rendering the component
    render() {
        // Rendering for mobile
        if (this.state.innerWidth <= 1000) {
            // Returning the jsx component
            return(
                <Fragment>
                    <section className="registerMainDiv">
                        {/* Adding the display message for the register page */}
                        <div className="registerDisplayHeaderDiv">
                            {/* {this.state.status && (
                                <FlashMessage duration={5000}>
                                    <div className="flashMessageDiv">
                                        <p> {this.state.statusMessage} </p>
                                    </div>
                                </FlashMessage>
                            )} */}
                            <div className="flashMessageDiv" id="flashMessageDiv">
                                <p> {this.state.statusMessage} </p>

                            </div>
                            <h1 className="registerSignUpHeader"> Register </h1>
                            <p className="registerDisplayHeaderPara"> Please sign up to get started </p>

                        </div>

                        {/* Adding the register div */}
                        <div className="registerDiv">
                            <div>
                                <label className="labelHeader"> FULLNAME </label> <br/>
                                <input type="text" placeholder='Fullname' className="inputForm" id="fullname" onClick={this.removeFlashMessage} />
                            </div>

                            <div>
                                <label className="labelHeader"> EMAIL </label> <br />
                                <input type="email" placeholder='Email address' className="inputForm" id="emailAddress" onClick={this.removeFlashMessage} />
                            </div>

                            <div>
                                <label className="labelHeader"> PHONE NUMBER </label> <br />
                                <input type="tel" placeholder='Phone number' className="inputForm" id="phoneNumber" onClick={this.removeFlashMessage} />
                            </div>

                            <div>
                                <label className="labelHeader"> PASSWORD </label> <br />
                                <input type="password" placeholder='Password' className="inputForm" id="password" onClick={this.removeFlashMessage} />
                            </div>

                            <div>
                                <label className="labelHeader"> RE-TYPE PASSWORD </label> <br />
                                <input type="password" placeholder='Retype password' className="inputForm" id="passwordConfirmation" onClick={this.removeFlashMessage} />
                            </div>

                            <div className="registerBtnDiv">
                                <button className="registerBtn" onClick={this.handleRegister}> Register </button>
                            </div>

                            <div className="loginDontHaveAnAccountDiv">
                                <p> Already have an account ? <Link to='/login' className="loginLinkTag forgotPasswordLink"> Login </Link></p>
                            </div>

                        </div>
                    </section>
                </Fragment>
            )

        }

        // Else for PC and Tablet devices
        else {
            // Returning the jsx component
            return(
                <Fragment>
                    <p> Register PC-Page </p>
                </Fragment>
            )
        }

    }
}

// Exporting the Register component
export default Register;




























// Sending the data to the backend server on
// route /register
// fetch('http://192.168.43.95:3001/register', {
//     method: 'POST',
//     headers: {
//         'Content-Type': 'application/json; charset=UTF-8'
//     },
//     body: userData,
//     mode: 'cors',
//     credentials: 'include',
// })
// .then((response) => response.json())
// .then((responseData) => {
//     // Handle the response data
//     if (responseData.status === "success") {
//         // User registered on the database
//         sweetAlert("success", "User Registered", responseData.message);

//         // Redirecting the user to the login page
//         setTimeout(() => {
//             window.location.href = '/login';
//         }, 3000);
//     }

//     // Else if there was an error in registration
//     else if (responseData.status === "error") {
//         // Error in registration
//         sweetAlert(responseData.status, 'Error registering user', responseData.message);

//     }
// })
// .catch((error) => {
//     console.log(error);
// })
