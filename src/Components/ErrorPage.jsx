// Importing the necessary modules 
import React, { Component, Fragment, useEffect } from 'react'; 
import { Link } from 'react-router-dom';
import "../Styles/ErrorPage.css"; 
import LoaderComponent from './LoaderComponent';

// Creating the class based component 
class ErrorPage extends Component {
    // Setting the state 
    state = {
        isLoading: true, 
        innerHeight: null, 
    } 

    // Component did mount 
    componentDidMount() {
        // Getting the inner height 
        const innerHeight = window.innerHeight; 

        // Setting the state 
        this.setState({
            innerHeight: innerHeight, 
        })
    }

    // Creating a function for showing the loader 
    showLoader = () => {
        // Setting the state 
        this.setState({ isLoading: true })
    }

    // Rendering the component 
    render() {
        
        // Returning the jsx component 
        return(
            <Fragment> 
                {/* Adding the loader component */}
                <LoaderComponent innerHeight={this.state.innerHeight}/> 
            </Fragment>
        )
    }
}

// Exporting the ErrorPage component 
export default ErrorPage; 