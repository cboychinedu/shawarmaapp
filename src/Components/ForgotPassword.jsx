// Importing the necessary modules 
import React, { Component, useState, Fragment } from 'react'; 
import { Link } from 'react-router-dom';
import "../Styles/Login.css";
import { flashMessageFunction } from './FlashMessage';

// Creating the functional component 
const ForgotPassword = (props) => {
    // Setting the state
    const [state, setState] = useState({
        innerWidth: null,
        innerHeight: null,
        statusMessage: "",
        isLoading: false,
      });

    // Handle forgot password 
    const handleForgotPassword = (event) => {
        // Preventing the default submission 
        event.preventDefault()

        // Getting the dom element from the forgot password 
        const emailAddress = document.getElementById("emailAddress"); 
        const flashMessageDiv = document.getElementById("flashMessageDiv");

        // Checking if the email address is valid 
        if (emailAddress.value === "") {
            setState({
                statusMessage: "Email address is required",
            })

            // Opening the flash message
            flashMessageFunction(flashMessageDiv, emailAddress);
        }
    }
    
    // return the jsx
    return(
        <Fragment>
            {/* Adding the Display Message Div */}
            <div className="loginDisplayHeaderDiv">
                <div className="flashMessageDiv" id="flashMessageDiv">
                    <p> {state.statusMessage} </p>

                </div>
                <h1 className="loginDisplayHeader"> Forgot Password </h1>
                <p className="loginDisplayPara"> Please sign in to your existing account </p>
            </div>

            {/* Adding the forgot password div */}
            <div className='loginDiv'>
                <div>
                    <label className="labelHeader"> EMAIL </label>
                    <input type="email" placeholder='Email address' className="inputForm" id="emailAddress"  />
                </div>
                <div>
                    <button className='loginBtn' onClick={handleForgotPassword}> SEND CODE </button>
                </div>
            </div>


        </Fragment>
    )
}

// Exporting the component 
export default ForgotPassword; 