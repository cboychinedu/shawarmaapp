// Importing the necessary modules 
import React, { Component, Fragment, useEffect } from 'react'; 
import { Link } from 'react-router-dom';
import items from './Items';
import homeLogo from '../Assets/Images/Home/homeLogo.jpg'; 
import "../Styles/Home.css"; 


// Creating the class based component 
class Home extends Component {
    // Setting the state 
    state = {
        currentIndex: 0, 
        innerWidth: null, 
        innerHeight: null, 
        buttonDisplay: "NEXT", 
        lat: 1.00, 
    }

    // Component did mount 
    componentDidMount() {
        // Getting the innerWidth, and innerHeight 
        const innerWidth = window.innerWidth; 
        const innerHeight = window.innerHeight; 

        // Setting the state 
        this.setState({
            innerWidth: innerWidth
        })        

        // Getting the user's location 
        window.navigator.geolocation.getCurrentPosition((position) => {
            // Setting the state 
            this.setState({
                lat: position.coords.latitude, 
            }) 

        }); 

    } 

    // Next Item 
    nextItem = (event) => {
        if (this.state.currentIndex < items.length -1) {
            // 
            setTimeout(() => {
                // 
                this.setState({
                    currentIndex: this.state.currentIndex + 1, 
                    buttonDisplay: "GET STARTED", 
                }); 

            }, 1000)

        }

        // 
        if (this.state.currentIndex === items.length -1) {
            // Redirecting the user to the Register screen, 
            // and delay for 500 mili-seconds 
            setTimeout(() => {
                window.location.href = '/register'; 
            }, 500);
            
        }
    }

    // Rendering the component 
    render() {
        // Rendering for mobile 
        if (this.state.innerWidth <= 1000) {
            // Returning the jsx component 
            return(
                <Fragment> 
                    <section className="homeMainDiv " > 
                        {/* Adding the Home Logo div */}
                        <div className="homeLogoDiv">
                            <img src={items[this.state.currentIndex].homeLogo} className="homeLogo" /> 

                        </div>

                        {/* Adding the Header div */}
                        <div className="headerDiv">
                            <h1 className="header"> {items[this.state.currentIndex].headerText} </h1> 

                        </div>

                    

                        {/* Adding the description para div */}
                        <div className="descriptionParagraphDiv">
                            <p className="descPara"> 
                                {items[this.state.currentIndex].headerPara}
                            </p>
                            <p className="descPara"> 
                                {items[this.state.currentIndex].headerParaTwo}
                            </p>
                            <p className="descPara"> 
                                {items[this.state.currentIndex].headerParaThree}
                            </p>
                        </div>

                        {/* Adding the buttons div */}
                        <div className="buttonsDiv">
                            <button 
                            className="getStartedBtn"
                            onClick={() => {
                                // 
                                this.nextItem(); 
                            }}
                            > {this.state.buttonDisplay} </button> 
                            <button className="skipBtn" onClick={() => {
                                // Delay for 500 mili-seconds 
                                setTimeout(() => {
                                    // Redirecting the user to the Login Screen
                                    window.location.href = '/login'; 
                                }, 500); 

                            }}> LOGIN </button>
                        </div>
                    </section>
                </Fragment>
            )

        } 

        // else for PC and Tablet devices 
        else {
            // Returning the jsx component 
            return(
                <Fragment> 
                    <p> PC-Page </p>
                </Fragment>
            )
        }
        
    }
}

// Exporting the Home component 
export default Home; 