// Importing the necessary modules 
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { AuthContextProvider } from './Auth/AuthContext';

// Rendering the main root component 
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <AuthContextProvider>
      <App /> 
    </AuthContextProvider>
);

// const root = document.getElementById('root');

// // 
// ReactDOM.render(
//   <React.StrictMode>
//     <AuthContextProvider>
//       <App /> 
//     </AuthContextProvider>
//   </React.StrictMode>, 
//   root 
// )


