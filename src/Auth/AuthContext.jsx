// Importing the necessary modules 
import React, { Component, createContext, Fragment } from 'react';

// Creating the AuthContext object 
const AuthContext = createContext(); 

// Creating the AuthContextProvider class 
class AuthContextProvider extends Component {
    // Setting the state 
    state = {
        isLoggedIn: false, 
        xAuthToken: null, 
        gpsLocation: {
            latitude: 0.00, 
            longitude: 0.00
        }, 
        cartItems: [],  
    }

    // Creating a function for adding the real cart items 
    addCartItems = (selectedCart) => {
        // Create a copy of the current cartItems array 
        let updatedCartItems = [...this.state.cartItems];

        // Add the new item to the copy 
        updatedCartItems.push(selectedCart.target.id); 

        // Setting the state 
        this.state.cartItems = updatedCartItems; 


        // 
        console.log(this.state.cartItems); 
    }

    // Creating a function for changing the xAuthToken state 
    setToken = (xTokenValue) => {
        // Setting the state if the user's token was validated 
        this.setState({
            isLoggedIn: true, 
            xAuthToken: xTokenValue, 
        })
    }

    // Creating a function for logging the user or removing the 
    // user's token 
    removeToken = () => {
        // Changing the state 
        this.setState({
            isLoggedIn: false, 
            xAuthToken: null, 
        })
    }

    // Creating a function for getting the user's gps constantly 
    getLocation = () => {
        // GPS Fn (Gps Function)
    }

    // Rendering the AuthContextProvider 
    render() {
        // Return the AuthContext Provider 
        return(
            <Fragment> 
                <AuthContext.Provider 
                    value={{
                        ...this.state, 
                        setToken: this.setToken, 
                        removeToken: this.removeToken, 
                        getLocation: this.getLocation,
                        addCartItems: this.addCartItems, 
                    }} >
                    { this.props.children }
                </AuthContext.Provider>
            </Fragment>
        )
    }
}

// Exporting the AuthContext 
export {
    AuthContext, 
    AuthContextProvider
}




//    // Creating a function for adding items to the cart state 
//    addItemIdToCart = (item) => {
//     // Create a copy of the current cartItems array 
//     let updatedCartItems = [...this.state.cartIdItems];

//     // Add the new item to the copy 
//     updatedCartItems.push(item.target.id); 
//     console.log(updatedCartItems); 

//     // 
//     this.state.cartItems = updatedCartItems

//     // 
//     console.log(this.state.cartItems); 

//     // 
//     // productsItems.forEach((item, index) => {

//     //     updatedCartItems.forEach((itemKey) => {
//     //         if (itemKey === item.id) {
//     //             // 
//     //             filteredProducts.push(item); 
//     //         }
//     //     })
            
//     // })

//     // // console.log(filteredProducts); 
//     // this.state.cartItems = filteredProducts; 

   
// }