// Importing the necessary modules 
import Home from './Components/Home';
import Login from './Components/Login'; 
import About from './Components/About'; 
import Register from './Components/Register'; 
import Dashboard from './Components/DashboardPages/Dashboard';
import ErrorPage from './Components/ErrorPage';  
import TrackOrder from './Components/DashboardPages/TrackOrder';
import AddDebitCard from './Components/DashboardPages/AddDebitCard';
import Profile from './Components/DashboardPages/Profile';
import Setting from './Components/Setting';
import React, { Component } from 'react'; 
import { AuthContext } from './Auth/AuthContext';
import CartContainer from './Components/DashboardPages/CartContainer';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import ForgotPassword from './Components/ForgotPassword';

// Setting the token if present 
let tokenValue = localStorage.getItem('x-auth-token') || null; 

// Rendering the App component 
class App extends Component {
  // Getting the Auth context 
  static contextType = AuthContext; 

  // Setting the state 
  state = {} 

  // Rendering the App component 
  render() {
    // Getting the context data 
    const { isLoggedIn, xAuthToken, setToken } = this.context; 

    // Setting the token 
    setToken(tokenValue); 

    // If the token value is present, and the isLoggeIn condition is 
    // true, render the following 
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/forgotPassword" element={<ForgotPassword />} />

          {isLoggedIn && xAuthToken ? (
            <>
              <Route path="/dashboard" element={<Dashboard />} />
              <Route path="trackOrder" element={<TrackOrder />} /> 
              <Route path="/profile" element={<Profile />} /> 
              <Route path="/addDebitCard" element={<AddDebitCard />} />
              <Route path="/setting" element={<Setting />} /> 
              <Route path="/cartPage/:token" element={<CartContainer />} /> 
              
              <Route path="*" element={<Navigate to="/dashboard" />} />
            </>
          ) : (
            <>
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="*" element={<ErrorPage />} />
            </>
          )}
        </Routes>
      </BrowserRouter>
    );
    
  }
}

// Exporting the App component 
export default App; 
