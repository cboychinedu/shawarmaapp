// config-overrides.js
const path = require('path');

module.exports = function override(config, env) {
  // Add the 'babel-plugin-module-resolver' to the Babel plugins.
  config.module.rules[1].oneOf[1].options.plugins.push([
    'module-resolver',
    {
      alias: {
        '@components': path.resolve(__dirname, 'src/Components'),
      },
    },
  ]);

  return config;
};
